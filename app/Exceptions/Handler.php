<?php

namespace App\Exceptions;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Auth\AuthenticationException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Symfony\Component\HttpKernel\Exception\HttpException;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    // protected $dontReport = [
    //     //
    // ];

    protected $dontReport = [
        \Illuminate\Auth\AuthenticationException::class,
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Validation\ValidationException::class,
    ];

        // Sentry report

        public function report(Throwable $exception)
        {
            if (app()->bound('sentry') && $this->shouldReport($exception)) {
                app('sentry')->captureException($exception);
            }
            parent::report($exception);
        }

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->renderable(function(Throwable $exception, Request $request){
            return $this->handleExceptions($exception, $request);
      });

        // $this->reportable(function (Throwable $e) {
        //     //
        // });
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Throwable  $e
     * @param  \Illuminate\Http\Request  $request
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function handleExceptions(Throwable $exception, Request $request){

        if($exception instanceof HttpException){
           $statusCode = $exception->getStatusCode();
           return $this->jsonResponse(Response::$statusTexts[$statusCode], $statusCode);
        }else if($exception instanceof AuthenticationException || $exception instanceof AuthorizationException ){
            // error_log($request); // add this line to know the actual error

            return $this->jsonResponse($exception->getMessage(), Response::HTTP_UNAUTHORIZED);
        }
        else if($exception instanceof ValidationException){
            $error = collect($exception->validator->getMessageBag())->flatten();
            return $this->jsonResponse($error, Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        //TODO: Handle more exception types

        return $this->jsonResponse($exception->getMessage(), 500);
   }

    /**
     * respond with a json format
     * @param string $message
     * @param $status_code
     * @return mixed
     */
    protected function jsonResponse($message  = 'There was an error', $status_code=null)
    {
        return response()->json([
            'error' => [
                'error'         => true,
                'message'       => $message,
                'status_code'   => $status_code
            ]
        ], $status_code);

    }
}

