<?php

namespace App\Http\Controllers;

use App\Models\UserWallet;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Resources\WalletResource;
use App\Http\Resources\TransactionResource;

class WalletController extends Controller
{
    public function MyWallet(Request $request){
        $paginate = $request->paginate ?? 10;
        $user = auth()->user();
        $user_wallet = UserWallet::where('user_id',$user->id);
        return WalletResource::collection($user_wallet->paginate($paginate));
    }

    public function AddCashToWallet(Request $request){
        $request->validate([
            'amount' => 'required|Integer',
            // 'from' => 'string',
        ]);

        $paginate = $request->paginate ?? 10;
        $user = auth()->user();
        $user_wallet = UserWallet::where('user_id',$user->id)->first();
        $bal = $user_wallet->balance + $request->amount;

        $user_wallet->update([
            'balance' => $bal
        ]);
        return WalletResource::collection($user_wallet->paginate($paginate));
    }

    public function SendMoney(Request $request){
        $request->validate([
            'amount' => 'required|Integer',
            'reciever_id' => 'required|exists:users,id',
        ]);

        $user = auth()->user();
        $user_wallet = UserWallet::where('user_id',$user->id)->first();
        // $bal = $user_wallet->balance
        if($user_wallet->balance < $request->amount)
            return response()->json(['status' => 'error', 'message' => 'Insuffficient fund'], 403);
        else{
            $reciever = User::find($request->reciever_id);
            $reciever_wallet = UserWallet::where('user_id',$reciever->id)->first();

            $bal = $user_wallet->balance - $request->amount;
            $reciever_bal = $reciever_wallet->balance + $request->amount;

            $user_wallet->update([
                'balance' => $bal
            ]);

            $reciever_wallet->update([
                'balance' => $reciever_bal
            ]);

            return response()->json(['status' => 'success', 'message' => '$'.$request->amount .' has been sent successfully. Thank you for banking with us'], 200);
        }
    }

    public function TransactionHistory(Request $request){
        $paginate = $request->paginate ?? 10;
        $user = auth()->user();
        $transactions = Transaction::where('user_id',$user->id);
        return TransactionResource::collection($transactions->paginate($paginate));
    }
}
