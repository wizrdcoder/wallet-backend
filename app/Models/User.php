<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use App\Models\UserWallet;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function wallet()
    // {
    //     return $this->belongsTo(UserWallet::class);
    // }

    public static function boot(){
        parent::boot();

        self::created(function ($model){

            $wallet = new UserWallet([
                'user_id' => $model->id,
                'balance'  => 0.00,
            ]);
            $wallet->save();

        });
    }
}
