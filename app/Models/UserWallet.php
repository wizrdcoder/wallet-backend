<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserWallet extends Model
{
    use HasFactory;

    protected $table='user_wallets';

    protected $fillable = [
        'user_id',
        'balance',
        'is_dormant',
    ];

    public static function boot(){
        parent::boot();

        self::updating(function ($model){
            if ($model->balance && $model->getOriginal('balance') != $model->balance) {

                $old_bal = $model->getOriginal('balance');
                $new_bal = $model->balance;
                $type = 'credit';
                if($old_bal > $new_bal){
                    # debit
                    $type = 'debit';
                }

                Transaction::create([
                    'user_id' => $model->user_id,
                    'amount' => $new_bal - $old_bal,
                    'type' => $type
                ]);

            }
            // $old_balance = $model->getOri
        });
    }
}
