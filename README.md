## Set Up

>**IMPORTANT** </br>
> **DO NOT MODIFY AN EXISTING MIGRATION FILE**
> If you need to modify an existing column or add a new column in a table create a new migration for that 

### Install Composer Packages 

```bash
composer install
```

### Set up `.env` file

```bash
cp .env.example .env
```

This will duplicate the contents of `.env.example` into `.env`. Edit `.env` with your preferred IDE or text editor and fill in your correct Database credentials.

### Generate application key

```bash
php artisan key:generate
```

### Run Migrations and seed

```bash
php artisan migrate --seed
```

### Install Passport 

```bash
php artisan passport:install
```

# Staging 

API LIVE URL: http://

## Note from wizrdcoder

* Comments are added where necessary
* All methods are documented
* Take your time to understand the existing structure 

## Concern(s)

This is just backend Restful API for the wallet app. the frontend written in ReactJS is in anotherr repository. 


## Unit Testing 

unit Testing has been written for all endpoint. run the follow command from project directory to run the test

```bash
./vendor/bin/phpunit
```
