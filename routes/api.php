<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup')->name('wallet.user.signup');
    Route::get('users', 'AuthController@allusers');

    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        // Route::get('logout', 'AuthController@logout');
        Route::get('wallet', 'WalletController@MyWallet');
        Route::post('wallet/add', 'WalletController@AddCashToWallet')->name('fund_wallet');
        Route::post('wallet/send', 'WalletController@SendMoney')->name('send_money');
        Route::get('wallet/history', 'WalletController@TransactionHistory')->name('transaction_history');;;
    });
});
