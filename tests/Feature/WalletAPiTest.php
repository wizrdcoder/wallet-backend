<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Models\User;
use App\Models\UserWallet;
use App\Models\Transaction;

use function PHPUnit\Framework\assertJson;

class WalletAPiTest extends TestCase
{
    use RefreshDatabase;
    // use HasFactory;


    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_can_create_user_task()
    {
        $formData = [
            'first_name' => 'John',
            'last_name' => 'Doe',
            'email' => 'test@gmail.com',
            'password' => 'password',
            'password_confirmation' => 'password'
        ];
        $this->withoutExceptionHandling();
        $this->json('POST',route('wallet.user.signup'), $formData)
                ->assertStatus(201);
    }

    public function test_can_fund_wallet_task()
    {
        $user = User::factory()->create();

        $this->actingAs($user, 'api');

        $formData = [
            'amount' => 10000
        ];

        $this->withoutExceptionHandling();
        $this->json('POST',route('fund_wallet'), $formData)
                ->assertStatus(200);
    }

    public function test_can_send_money_task()
    {
        $user = User::factory()->create();
        $reciever_user = User::factory()->create();

        $sender_wallet = UserWallet::where('user_id',$user->id)->first();
        $sender_wallet->update([
            'balance' => 1000,
        ]);

        $this->actingAs($user, 'api');

        $formData = [
            'amount' => 100,
            'reciever_id' => $reciever_user->id
        ];

        $this->withoutExceptionHandling();
        $this->json('POST',route('send_money'), $formData)
                ->assertStatus(200);
    }

    public function test_can_view_transaction_history_task()
    {
        $user = User::factory()->create();

        $transactions = Transaction::where('user_id',$user->id);


        $this->actingAs($user, 'api');

        $formData = [];

        $this->withoutExceptionHandling();
        $this->json('GET',route('transaction_history'), $formData)
                ->assertStatus(200);
    }
}
